From baf0e3d9a2c7d12661aeab819889c721699d32df Mon Sep 17 00:00:00 2001
From: Jack Xu <jackyzy823@gmail.com>
Date: Tue, 29 Oct 2024 21:14:52 +0800
Subject: [PATCH] Add support for freerdp3

---
 src/Cursor.cpp           | 11 +++--
 src/InputHandler.cpp     |  4 ++
 src/NetworkDetection.cpp | 43 +++++++++++++++++++
 src/NetworkDetection.h   |  8 ++++
 src/RdpConnection.cpp    | 91 +++++++++++++++++++++-------------------
 src/VideoStream.cpp      |  4 +-
 6 files changed, 112 insertions(+), 49 deletions(-)

--- a/src/Cursor.cpp
+++ b/src/Cursor.cpp
@@ -79,7 +79,7 @@
     }
 
     auto context = d->session->rdpPeerContext();
-    auto updatePointer = d->session->rdpPeer()->update->pointer;
+    auto updatePointer = d->session->rdpPeerContext()->update->pointer;
 
     // Cursor images are cached. Check to see if the newly requested cursor is
     // already in the cache, and if so, mark that as the current cursor.
@@ -104,7 +104,7 @@
     newCursor.lastUsed = std::chrono::steady_clock::now();
 
     // Evict least recently used cursor from the cache if it has grown too large.
-    if (d->cursorCache.size() >= d->session->rdpPeer()->settings->PointerCacheSize) {
+    if (d->cursorCache.size() >= freerdp_settings_get_uint32(d->session->rdpPeerContext()->settings, FreeRDP_PointerCacheSize)) {
         auto lru = std::min_element(d->cursorCache.cbegin(), d->cursorCache.cend(), [](const CursorUpdate &first, const CursorUpdate &second) {
             return first.lastUsed < second.lastUsed;
         });
@@ -117,9 +117,14 @@
         pointerNewUpdate.xorBpp = 32;
         auto &colorUpdate = pointerNewUpdate.colorPtrAttr;
         colorUpdate.cacheIndex = newCursor.cacheId;
+#ifdef FREERDP3
+        colorUpdate.hotSpotX = newCursor.hotspot.x();
+        colorUpdate.hotSpotY = newCursor.hotspot.y();
+#else
         // xPos and yPos are actually the hotspot coordinates, not position.
         colorUpdate.xPos = newCursor.hotspot.x();
         colorUpdate.yPos = newCursor.hotspot.y();
+#endif
         colorUpdate.width = newCursor.image.width();
         colorUpdate.height = newCursor.image.height();
         colorUpdate.lengthAndMask = 0;
@@ -165,7 +170,7 @@
         d->lastUsedCursor = nullptr;
         POINTER_SYSTEM_UPDATE pointerSystemUpdate;
         pointerSystemUpdate.type = type == CursorType::Hidden ? SYSPTR_NULL : SYSPTR_DEFAULT;
-        d->session->rdpPeer()->update->pointer->PointerSystem(d->session->rdpPeerContext(), &pointerSystemUpdate);
+        d->session->rdpPeerContext()->update->pointer->PointerSystem(d->session->rdpPeerContext(), &pointerSystemUpdate);
     }
 }
 
--- a/src/InputHandler.cpp
+++ b/src/InputHandler.cpp
@@ -178,7 +178,11 @@
     auto virtualCode = GetVirtualKeyCodeFromVirtualScanCode(flags & KBD_FLAGS_EXTENDED ? code | KBDEXT : code, 4);
     virtualCode = flags & KBD_FLAGS_EXTENDED ? virtualCode | KBDEXT : virtualCode;
     // While "type" suggests an EVDEV code, the actual code is an X code
+#ifdef FREERDP3
+    quint32 keycode = GetKeycodeFromVirtualKeyCode(virtualCode, WINPR_KEYCODE_TYPE_EVDEV) - 8;
+#else
     quint32 keycode = GetKeycodeFromVirtualKeyCode(virtualCode, KEYCODE_TYPE_EVDEV) - 8;
+#endif
 
     auto type = flags & KBD_FLAGS_DOWN ? QEvent::KeyPress : QEvent::KeyRelease;
 
--- a/src/NetworkDetection.cpp
+++ b/src/NetworkDetection.cpp
@@ -28,6 +28,16 @@
 constexpr auto rttAverageInterval = clk::milliseconds(500);
 constexpr auto networkResultInterval = clk::seconds(1);
 
+#ifdef FREERDP3
+BOOL rttMeasureResponse(rdpAutoDetect *rdpAutodetect, RDP_TRANSPORT_TYPE, uint16_t sequence)
+{
+    auto context = reinterpret_cast<PeerContext *>(rdpAutodetect->context);
+    if (context->networkDetection->onRttMeasureResponse(sequence)) {
+        return TRUE;
+    }
+    return FALSE;
+}
+#else
 BOOL rttMeasureResponse(rdpContext *rdpContext, uint16_t sequence)
 {
     auto context = reinterpret_cast<PeerContext *>(rdpContext);
@@ -36,7 +46,18 @@
     }
     return FALSE;
 }
+#endif
 
+#ifdef FREERDP3
+BOOL bwMeasureResults(rdpAutoDetect *rdpAutodetect, RDP_TRANSPORT_TYPE, uint16_t, uint16_t, uint32_t, uint32_t)
+{
+    auto context = reinterpret_cast<PeerContext *>(rdpAutodetect->context);
+    if (context->networkDetection->onBandwidthMeasureResults()) {
+        return TRUE;
+    }
+    return FALSE;
+}
+#else
 BOOL bwMeasureResults(rdpContext *rdpContext, uint16_t)
 {
     auto context = reinterpret_cast<PeerContext *>(rdpContext);
@@ -45,6 +66,7 @@
     }
     return FALSE;
 }
+#endif
 
 struct RTTMeasurement {
     clk::system_clock::time_point measurementTime;
@@ -109,7 +131,11 @@
     }
 
     d->state = State::PendingStop;
+#ifdef FREERDP3
+    d->rdpAutodetect->BandwidthMeasureStart(d->rdpAutodetect, RDP_TRANSPORT_TCP, 0);
+#else
     d->rdpAutodetect->BandwidthMeasureStart(d->rdpAutodetect->context, 0);
+#endif
 }
 
 void NetworkDetection::stopBandwidthMeasure()
@@ -119,7 +145,11 @@
     }
 
     d->state = State::PendingResults;
+#ifdef FREERDP3
+    d->rdpAutodetect->BandwidthMeasureStop(d->rdpAutodetect, RDP_TRANSPORT_TCP, 0, 0);
+#else
     d->rdpAutodetect->BandwidthMeasureStop(d->rdpAutodetect->context, 0);
+#endif
 }
 
 void NetworkDetection::update()
@@ -137,7 +167,11 @@
 
     auto sequence = d->nextSequenceNumber();
     d->rttRequests.insert(sequence, now);
+#ifdef FREERDP3
+    d->rdpAutodetect->RTTMeasureRequest(d->rdpAutodetect, RDP_TRANSPORT_TCP, sequence);
+#else
     d->rdpAutodetect->RTTMeasureRequest(d->rdpAutodetect->context, sequence);
+#endif
 }
 
 bool NetworkDetection::onRttMeasureResponse(uint16_t sequence)
@@ -213,10 +247,19 @@
 
     d->lastNetworkResult = now;
 
+#ifdef FREERDP3
+    rdpNetworkCharacteristicsResult result;
+    result.type = RDP_NETCHAR_RESULT_TYPE_BASE_RTT_BW_AVG_RTT;
+    result.baseRTT = clk::duration_cast<clk::milliseconds>(d->minimumRtt).count();
+    result.averageRTT = clk::duration_cast<clk::milliseconds>(d->averageRtt).count();
+    result.bandwidth = d->lastBandwithMeasurement;
+    d->rdpAutodetect->NetworkCharacteristicsResult(d->rdpAutodetect, RDP_TRANSPORT_TCP, d->nextSequenceNumber(), &result);
+#else
     d->rdpAutodetect->netCharBandwidth = d->lastBandwithMeasurement;
     d->rdpAutodetect->netCharBaseRTT = clk::duration_cast<clk::milliseconds>(d->minimumRtt).count();
     d->rdpAutodetect->netCharAverageRTT = clk::duration_cast<clk::milliseconds>(d->averageRtt).count();
     d->rdpAutodetect->NetworkCharacteristicsResult(d->rdpAutodetect->context, d->nextSequenceNumber());
+#endif
 }
 
 uint32_t NetworkDetection::Private::nextSequenceNumber()
--- a/src/NetworkDetection.h
+++ b/src/NetworkDetection.h
@@ -10,6 +10,9 @@
 #include <QObject>
 
 #include <freerdp/freerdp.h>
+#ifdef FREERDP3
+#include <freerdp/types.h>
+#endif
 
 namespace KRdp
 {
@@ -46,8 +49,13 @@
     void update();
 
 private:
+#ifdef FREERDP3
+    friend BOOL rttMeasureResponse(rdpAutoDetect *, RDP_TRANSPORT_TYPE, uint16_t);
+    friend BOOL bwMeasureResults(rdpAutoDetect *, RDP_TRANSPORT_TYPE, uint16_t, uint16_t, uint32_t, uint32_t);
+#else
     friend BOOL rttMeasureResponse(rdpContext *, uint16_t);
     friend BOOL bwMeasureResults(rdpContext *, uint16_t);
+#endif
 
     bool onRttMeasureResponse(uint16_t sequence);
     bool onBandwidthMeasureResults();
--- a/src/RdpConnection.cpp
+++ b/src/RdpConnection.cpp
@@ -286,16 +286,16 @@
     }
 
 #ifdef FREERDP3
-    auto certificate = freerdp_certificate_new_from_file(d->server->tlsCertificate().toUtf8().data());
+    auto certificate = freerdp_certificate_new_from_file(d->server->tlsCertificate().string().data());
     if (!certificate) {
-        qCWarning(KRDP) << "Could not read certificate file" << d->server->tlsCertificate();
+        qCWarning(KRDP) << "Could not read certificate file" << d->server->tlsCertificate().string();
         return;
     }
     freerdp_settings_set_pointer_len(settings, FreeRDP_RdpServerCertificate, certificate, 1);
 
-    auto key = freerdp_key_new_from_file(d->server->tlsCertificateKey().toUtf8().data());
+    auto key = freerdp_key_new_from_file(d->server->tlsCertificateKey().string().data());
     if (!key) {
-        qCWarning(KRDP) << "Could not read certificate file" << d->server->tlsCertificate();
+        qCWarning(KRDP) << "Could not read certificate file" << d->server->tlsCertificate().string();
         return;
     }
     freerdp_settings_set_pointer_len(settings, FreeRDP_RdpServerRsaKey, key, 1);
@@ -307,50 +307,51 @@
     // Only NTLM Authentication (NLA) security is currently supported. This also
     // happens to be the most secure one. It implicitly requires a TLS
     // connection so the above certificate is always required.
-    settings->RdpSecurity = false;
-    settings->TlsSecurity = false;
-    settings->NlaSecurity = true;
+    freerdp_settings_set_bool(settings, FreeRDP_RdpSecurity, false);
+    freerdp_settings_set_bool(settings, FreeRDP_TlsSecurity, false);
+    freerdp_settings_set_bool(settings, FreeRDP_NlaSecurity, true);
 
-    settings->OsMajorType = OSMAJORTYPE_UNIX;
+    freerdp_settings_set_uint32(settings, FreeRDP_OsMajorType, OSMAJORTYPE_UNIX);
     // PSEUDO_XSERVER is apparently required for things to work properly.
-    settings->OsMinorType = OSMINORTYPE_PSEUDO_XSERVER;
+    freerdp_settings_set_uint32(settings, FreeRDP_OsMinorType, OSMINORTYPE_PSEUDO_XSERVER);
 
     // TODO: Implement audio support
-    settings->AudioPlayback = false;
+    freerdp_settings_set_bool(settings, FreeRDP_AudioPlayback, false);
 
-    settings->ColorDepth = 32;
+    freerdp_settings_set_uint32(settings, FreeRDP_ColorDepth, 32);
 
     // Plain YUV420 AVC is currently the most straightforward of the the AVC
     // related codecs to implement. Moreover, it makes the encoding side also
     // simpler so it is currently the only supported codec. This uses the RdpGfx
     // pipeline, so make sure to request that.
-    settings->SupportGraphicsPipeline = true;
-    settings->GfxAVC444 = false;
-    settings->GfxAVC444v2 = false;
-    settings->GfxH264 = true;
-
-    settings->GfxSmallCache = false;
-    settings->GfxThinClient = false;
-
-    settings->HasExtendedMouseEvent = true;
-    settings->HasHorizontalWheel = true;
-    settings->UnicodeInput = true;
+    freerdp_settings_set_bool(settings, FreeRDP_SupportGraphicsPipeline, true);
+    freerdp_settings_set_bool(settings, FreeRDP_GfxAVC444, false);
+    freerdp_settings_set_bool(settings, FreeRDP_GfxAVC444v2, false);
+    freerdp_settings_set_bool(settings, FreeRDP_GfxH264, true);
+
+
+    freerdp_settings_set_bool(settings, FreeRDP_GfxSmallCache, false);
+    freerdp_settings_set_bool(settings, FreeRDP_GfxThinClient, false);
+
+    freerdp_settings_set_bool(settings, FreeRDP_HasExtendedMouseEvent, true);
+    freerdp_settings_set_bool(settings, FreeRDP_HasHorizontalWheel, true);
+    freerdp_settings_set_bool(settings, FreeRDP_UnicodeInput, true);
 
     // TODO: Implement network performance detection
-    settings->NetworkAutoDetect = true;
+    freerdp_settings_set_bool(settings, FreeRDP_NetworkAutoDetect, true);
 
-    settings->RefreshRect = true;
-    settings->RemoteConsoleAudio = true;
-    settings->RemoteFxCodec = false;
-    settings->NSCodec = false;
-    settings->FrameMarkerCommandEnabled = true;
-    settings->SurfaceFrameMarkerEnabled = true;
+    freerdp_settings_set_bool(settings, FreeRDP_RefreshRect, true);
+    freerdp_settings_set_bool(settings, FreeRDP_RemoteConsoleAudio, true);
+    freerdp_settings_set_bool(settings, FreeRDP_RemoteFxCodec, false);
+    freerdp_settings_set_bool(settings, FreeRDP_NSCodec, false);
+    freerdp_settings_set_bool(settings, FreeRDP_FrameMarkerCommandEnabled, true);
+    freerdp_settings_set_bool(settings, FreeRDP_SurfaceFrameMarkerEnabled, true);
 
     d->peer->Capabilities = peerCapabilities;
     d->peer->Activate = peerActivate;
     d->peer->PostConnect = peerPostConnect;
 
-    d->peer->update->SuppressOutput = suppressOutput;
+    d->peer->context->update->SuppressOutput = suppressOutput;
 
     d->inputHandler->initialize(d->peer->context->input);
     context->inputHandler = d->inputHandler.get();
@@ -393,18 +394,11 @@
             break;
         }
 
-        // Read data for the virtual channel manager.
-        // Note that this is separate from the above file handle for... some reason.
-        // However, if we don't call this, login and any dynamic channels will not work.
-        if (WTSVirtualChannelManagerCheckFileDescriptor(context->virtualChannelManager) != TRUE) {
-            qCDebug(KRDP) << "Unable to check Virtual Channel Manager file descriptor, closing connection";
-            break;
-        }
-
         // Initialize any dynamic channels once the dynamic channel channel is setup.
-        if (WTSVirtualChannelManagerIsChannelJoined(context->virtualChannelManager, DRDYNVC_SVC_CHANNEL_NAME)) {
+        if (d->peer->connected && WTSVirtualChannelManagerIsChannelJoined(context->virtualChannelManager, DRDYNVC_SVC_CHANNEL_NAME)) {
+            auto state = WTSVirtualChannelManagerGetDrdynvcState(context->virtualChannelManager);
             // Dynamic channels can only be set up properly once the dynamic channel channel is properly setup.
-            if (WTSVirtualChannelManagerGetDrdynvcState(context->virtualChannelManager) == DRDYNVC_STATE_READY) {
+            if (state == DRDYNVC_STATE_READY) {
                 if (d->videoStream->initialize()) {
                     d->videoStream->setEnabled(true);
                     setState(State::Streaming);
@@ -416,9 +410,17 @@
                         break;
                     }
                 }
+            } else if (state == DRDYNVC_STATE_NONE) {
+                // This ensures that WTSVirtualChannelManagerCheckFileDescriptor() will be called, which initializes the drdynvc channel.
+                SetEvent(channelEvent);
             }
         }
 
+        if (WaitForSingleObject(channelEvent, 0) == WAIT_OBJECT_0 && WTSVirtualChannelManagerCheckFileDescriptor(context->virtualChannelManager) != TRUE) {
+            qCDebug(KRDP) << "Unable to check Virtual Channel Manager file descriptor, closing connection";
+            break;
+        }
+
         d->networkDetection->update();
     }
 
@@ -431,22 +433,23 @@
     auto settings = d->peer->context->settings;
     // We only support GraphicsPipeline clients currently as that is required
     // for AVC streaming.
-    if (!settings->SupportGraphicsPipeline) {
+    if (!freerdp_settings_get_bool(settings, FreeRDP_SupportGraphicsPipeline)) {
         qCWarning(KRDP) << "Client does not support graphics pipeline which is required";
         return false;
     }
 
-    if (settings->ColorDepth != 32) {
-        qCDebug(KRDP) << "Correcting invalid color depth from client:" << settings->ColorDepth;
-        settings->ColorDepth = 32;
+    auto colorDepth = freerdp_settings_get_uint32(settings, FreeRDP_ColorDepth);
+    if (colorDepth != 32) {
+        qCDebug(KRDP) << "Correcting invalid color depth from client:" << colorDepth;
+        freerdp_settings_set_uint32(settings, FreeRDP_ColorDepth, 32);
     }
 
-    if (!settings->DesktopResize) {
+    if (!freerdp_settings_get_bool(settings, FreeRDP_DesktopResize)) {
         qCWarning(KRDP) << "Client doesn't support resizing, aborting";
         return false;
     }
 
-    if (settings->PointerCacheSize <= 0) {
+    if (freerdp_settings_get_uint32(settings,FreeRDP_PointerCacheSize) <= 0) {
         qCWarning(KRDP) << "Client doesn't support pointer caching, aborting";
         return false;
     }
--- a/src/VideoStream.cpp
+++ b/src/VideoStream.cpp
@@ -154,7 +154,7 @@
         return true;
     }
 
-    auto peerContext = reinterpret_cast<PeerContext *>(d->session->rdpPeer()->context);
+    auto peerContext = reinterpret_cast<PeerContext *>(d->session->rdpPeerContext());
 
     d->gfxContext = Private::RdpGfxContextPtr{rdpgfx_server_context_new(peerContext->virtualChannelManager), rdpgfx_server_context_free};
     if (!d->gfxContext) {
@@ -168,7 +168,7 @@
     d->gfxContext->QoeFrameAcknowledge = gfxQoEFrameAcknowledge;
 
     d->gfxContext->custom = this;
-    d->gfxContext->rdpcontext = d->session->rdpPeer()->context;
+    d->gfxContext->rdpcontext = d->session->rdpPeerContext();
 
     if (!d->gfxContext->Open(d->gfxContext.get())) {
         qCWarning(KRDP) << "Could not open GFX context";
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -37,9 +37,15 @@
 
 find_package(KF6 ${KF6_MIN_VERSION} REQUIRED COMPONENTS Config DBusAddons KCMUtils I18n CoreAddons StatusNotifierItem Crash)
 
-find_package(FreeRDP 2.10 REQUIRED)
-find_package(WinPR 2.10 REQUIRED)
-find_package(FreeRDP-Server 2.10 REQUIRED)
+if(WITH_FREERDP3)
+    find_package(FreeRDP 3.1 REQUIRED)
+    find_package(WinPR 3.1 REQUIRED)
+    find_package(FreeRDP-Server 3.1 REQUIRED)
+else()
+    find_package(FreeRDP 2.10 REQUIRED)
+    find_package(WinPR 2.10 REQUIRED)
+    find_package(FreeRDP-Server 2.10 REQUIRED)
+endif()
 find_package(KPipeWire 5.27.80 REQUIRED)
 find_package(XKB REQUIRED)
 
