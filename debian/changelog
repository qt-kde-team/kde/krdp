krdp (6.3.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.3.1).
  * New upstream release (6.3.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 28 Feb 2025 00:53:01 +0100

krdp (6.3.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.3.0).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 10 Feb 2025 14:59:39 +0100

krdp (6.2.91-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * Fix Vcs-* URLs. (Closes: #1092560)
  * New upstream release (6.2.91).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 23 Jan 2025 23:52:48 +0100

krdp (6.2.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.90).
  * Update build-deps and deps with the info from cmake.
  * Bump inter-plasma versioned dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 11 Jan 2025 23:38:46 +0100

krdp (6.2.5-2) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * Also remove libwinpr2-dev build dependency, shipped by FreeRDP 2.
    (Closes: #1081528)

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 10 Jan 2025 21:55:11 +0100

krdp (6.2.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * Backport upstream MR to add support for FreeRDP 3.
  * New upstream release (6.2.5).
  * Update build-deps and deps with the info from cmake.
  * Refresh patches.
  * Add build flag to build with FreeRDP 3.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 05 Jan 2025 11:21:46 +0100

krdp (6.2.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 03 Dec 2024 16:36:14 +0100

krdp (6.2.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.3).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 23 Nov 2024 21:57:30 +0100

krdp (6.2.1-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.1).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 15 Oct 2024 18:21:12 +0200

krdp (6.2.0-1) experimental; urgency=medium

  * New upstream release (6.1.90).
  * Update build-deps and deps with the info from cmake.
  * New upstream release (6.2.0).
  * Tighten inter-Plasma package dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 05 Oct 2024 23:17:46 +0200

krdp (6.1.5-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.5).
  * Update build-deps and deps with the info from cmake.
  * Tighten Plasma packages inter-dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 11 Sep 2024 23:28:00 +0200

krdp (6.1.4-2) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * Tighten Plasma packages inter-dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 18 Aug 2024 01:07:37 +0200

krdp (6.1.4-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.4).
  * Add Recommends to systemsettings for the RDP server configuration KCM.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 11 Aug 2024 23:58:22 +0200

krdp (6.1.3-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.3).

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 21 Jul 2024 23:46:18 +0200

krdp (6.1.0-1) experimental; urgency=medium

  * Initial release. (Closes: #1074485)

 -- Patrick Franz <deltaone@debian.org>  Sun, 30 Jun 2024 17:09:43 +0200
